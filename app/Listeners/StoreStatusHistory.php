<?php

namespace App\Listeners;

use App\Events\ChangeReceiptStatus;
use App\Models\Receipt;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StoreStatusHistory
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(ChangeReceiptStatus $event)
    {
        $current_timestamp = Carbon::now()->toDateTimeString();
        $receiptInfo = $event->receipt;

        return DB::table('receipt_change_logs')->insert([
            'admin_id' => Auth::id(),
            'receipt_id' => $receiptInfo->id,
            'status' => ($receiptInfo->status) === 'Có' ? 1 : 0,
            'note' => 'Change to ' . $receiptInfo->status,
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ]);
    }
}
