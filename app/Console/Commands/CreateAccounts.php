<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-accounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new account';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $username = $this->ask('What is your name?');
        $password = $this->secret('What is the password?');

        if ($username === '' || $password === '') {
            $this->error('username or password must not be blank');
        } else {
            $user = new User;
            $user->username = $username;
            $user->password = bcrypt($password);
            $user->api_token = Str::random(60);
            $user->api_expired_at = now()->addDays(30);
            $user->save();

            $this->info('Account created successfully');
        }

    }
}
