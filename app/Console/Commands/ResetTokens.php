<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ResetTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:reset-tokens {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset api_token of user';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $username  = $this->argument('username');
        $user = User::where('username', $username)->first();

        if ($user) {
            $user->api_token = Str::random(40);
            $user->api_expired_at = now()->addDays(30);
            $user->save();
            $this->info("Reset token of {$user->username} successfully!");
        } else {
            $this->error('User not found!');
        }

    }
}
