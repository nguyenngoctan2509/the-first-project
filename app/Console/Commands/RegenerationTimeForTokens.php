<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class RegenerationTimeForTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:regeneration-time-for-tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regeneration time for tokens of user';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $usernames = User::pluck('username')->all();
        $username = $this->anticipate('Which username you choose ?', $usernames);
        $user = User::where('username', $username)->first();

        if ($user) {
            $user->api_expired_at = now()->addDays(60);
            $user->save();
            $this->info("Regenerate time for token of {$user->username} successfully!");
        } else {
            $this->error('User not found!');
        }
    }
}
