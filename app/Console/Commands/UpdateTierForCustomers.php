<?php

namespace App\Console\Commands;

use App\Models\Customer;
use Illuminate\Console\Command;

class UpdateTierForCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-tier-for-customer {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update tier for customer';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $name  = $this->argument('name');
        $customer = Customer::where('name', $name)->first();
        if ($customer) {
            $tier = ($customer->totalMoney() < 1000000) ? 1 : (($customer->totalMoney() < 10000000) ? 2 : 3);
            $customer->tier = $tier;
            $customer->save();
            $this->info("Update tier for {$customer->name}successfully!");
        } else {
            $this->error('Customer not found!');
        }
    }
}
