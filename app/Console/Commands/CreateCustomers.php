<?php

namespace App\Console\Commands;

use App\Models\Customer;
use Illuminate\Console\Command;

class CreateCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new customer';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $name = $this->ask('What is your name?');
        $birthday = $this->ask('What is your birthday? (yyyy-mm-dd)');
        $email = $this->ask('What is your email?');

        if($name === '' || $email === ''){
            $this->error('name or email must not be blank');
        }else{
            $customer = new Customer;
            $customer->name = $name;
            $customer->birthday = $birthday;
            $customer->email = $email;
            $customer->save();

            $this->info('Customer created successfully');
        }

    }
}
