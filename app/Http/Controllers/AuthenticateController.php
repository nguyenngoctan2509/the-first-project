<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthenticateController extends Controller
{
    public function login(Request $request)
    {
        $credentials = [
            'username' => $request->username,
            'password' => $request->password
        ];
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $expirationTime = Carbon::createFromFormat('Y-m-d H:i:s', $user->api_expired_at);

            if ($user->api_token === null) {
                $user->api_token = Str::random(40);
                $user->save();
            } elseif ($user->api_expired_at === null) {
                $user->api_expired_at = now()->addDays(30);
                $user->save();
            } elseif ($expirationTime->isPast()) {
                Artisan::call('app:reset-tokens', [
                    'username' => $user->username
                ]);
            }
            return response()->json([
                'status' => Response::HTTP_OK,
                'data' => [
                    'token' => $user->api_token,
                    'expires_at' => $user->api_expired_at
                ]
            ], Response::HTTP_OK);
        }
        return response()->json([
            'status' => Response::HTTP_UNAUTHORIZED,
            'message' => 'Invalid credentials'
            ], Response::HTTP_UNAUTHORIZED);
    }
}
