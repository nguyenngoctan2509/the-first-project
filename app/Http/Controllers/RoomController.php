<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoomResource;
use App\Models\Room;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class RoomController extends Controller
{
    protected Room $room;

    /**
     * @param Room $room
     */
    public function __construct(Room $room)
    {
        $this->room = $room;
    }


    public function show($id): JsonResponse
    {
        $room = $this->room->findOrFail($id);
        $roomResource = new RoomResource($room);
        $error = $room ? '' : 'Not Found';

        return response()->json([
            'status' => Response::HTTP_OK,
            'data' => $roomResource,
            'error' => $error
        ]);
    }
}
