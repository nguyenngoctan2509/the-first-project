<?php

namespace App\Http\Controllers;

use App\Http\Resources\ServiceResource;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ServiceController extends Controller
{
    protected Service $service;

    /**
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function countServiceById($id, Request $request): \Illuminate\Http\JsonResponse
    {
        $token = $request->header('token');
        $tokenIsValid = User::where('api_token', $token)->first();
        if (empty($token)) {
            return response()->json([
                'status' => Response::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Token',
            ]);
        } elseif (empty($tokenIsValid)) {
            return response()->json([
                'status' => Response::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Token',
            ]);
        } else {
            $service = $this->service->findOrFail($id);
            $serviceResource = new ServiceResource($service);

            return response()->json([
                'status' => Response::HTTP_OK,
                'data' => $serviceResource,
            ]);
        }
    }
}
