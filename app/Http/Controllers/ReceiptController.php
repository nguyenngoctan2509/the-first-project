<?php

namespace App\Http\Controllers;

use App\Events\ChangeReceiptStatus;
use App\Http\Resources\ReceiptResource;
use App\Models\Receipt;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReceiptController extends Controller
{
    protected Receipt $receipt;

    /**
     * @param Receipt $receipt
     */
    public function __construct(Receipt $receipt)
    {
        $this->receipt = $receipt;
    }

    public function calcMoneyBillById($id): \Illuminate\Http\JsonResponse
    {
        $receipt = $this->receipt->findOrFail($id);
        $receiptResource = new ReceiptResource($receipt);

        return response()->json([
            'status' => Response::HTTP_OK,
            'data' => $receiptResource,
        ]);
    }

    public function changeStatusReceiptById(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $receipt = $this->receipt->findOrFail($id);
        $receipt->status = $request->input('status');

        if ($receipt->isDirty('status')) {
            $receipt->save();
            event(new ChangeReceiptStatus($receipt));
        }
        $receiptResource = new ReceiptResource($receipt);

        return response()->json([
            'status' => Response::HTTP_OK,
            'data' => $receiptResource,
        ], Response::HTTP_OK);
    }
}
