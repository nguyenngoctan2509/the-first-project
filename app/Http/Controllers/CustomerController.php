<?php

namespace App\Http\Controllers;

use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use Illuminate\Http\Response;

class CustomerController extends Controller
{
    protected Customer $customer;

    /**
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }


    public function getCustomerOrderedRoomInATimeRange($room_id, $start_time = null, $end_time = null): \Illuminate\Http\JsonResponse
    {
        $customer = Customer::whereHas('receipt.room', fn($query) => $query->where('room_id', $room_id)
            ->whereRaw('? <= receipt_room_details.check_in_time', [$start_time])
            ->whereRaw('? >= receipt_room_details.check_out_time', [$end_time])
        )->first();
        $customerResource = new CustomerResource($customer);

        return response()->json([
            'status' => Response::HTTP_OK,
            'data' => $customerResource,
        ]);

    }
}
