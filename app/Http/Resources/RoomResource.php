<?php

namespace App\Http\Resources;

use App\Models\ViewType;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RoomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'type' => new RoomTypeResource($this->roomType),
            'name' => $this->name,
            'status' => $this->status,
            'view' => new ViewTypeResource($this->viewType),
        ];
    }
}
