<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReceiptResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $money = $this->service->sum(function ($service) {
            return $service->pivot->quantity * $service->pivot->price;
        });
        return [
            'id' => $this->id,
            'money' => $money,
            'status' => $this->status,
            'note' => $this->note,
            'changes' => $this->receiptChangeLog
        ];
    }
}
