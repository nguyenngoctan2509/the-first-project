<?php

namespace App\Http\Middleware;

use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ApiTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response) $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->header('token');
        $tokenIsValid = User::where('api_token', $token)->first();
        Auth::loginUsingId($tokenIsValid->id);
        $expirationTime = Carbon::createFromFormat('Y-m-d H:i:s', $tokenIsValid->api_expired_at);

        if (empty($token)) {
            return response()->json([
                'status' => Response::HTTP_UNAUTHORIZED,
                'message' => 'Empty Token',
            ]);
        } elseif ($expirationTime ->isPast()) {
            return response()->json([
                'status' => Response::HTTP_UNAUTHORIZED,
                'message' => 'Token has expired',
            ]);
        } elseif (empty($tokenIsValid)) {
            return response()->json([
                'status' => Response::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Token',
            ]);
        } else {
            return $next($request);
        }
    }
}
