<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ViewType extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'view_types';
    protected $fillable=[
        'name',
        'price',
        'status',
    ];

    protected function status(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value ? 'Có' : 'Không' ,
        );
    }

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }
}
