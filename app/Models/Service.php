<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Service extends Model
{
    use HasFactory;
    protected $table = 'services';
    protected $fillable=[
        'name',
        'price',
        'unit',
        'status',
    ];

    protected function status(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value ? 'Có' : 'Không' ,
        );
    }
    public function receipt(): BelongsToMany
    {
        return $this->belongsToMany(Receipt::class,'receipt_service_details','service_id','receipt_id')
                    ->withPivot('price','quantity');
    }
}
