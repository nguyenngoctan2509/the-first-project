<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends \Illuminate\Foundation\Auth\User
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'users';
    protected $fillable=[
        'username',
        'password',
        'api_token',
        'api_expired_at',
    ];
}
