<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Receipt extends Model
{
    use HasFactory;
    protected $table = 'receipts';
    protected $fillable=[
        'status',
        'note',
    ];

    protected function status(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value ? 'Có' : 'Không' ,
        );
    }
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function service(): BelongsToMany
    {
        return $this->belongsToMany(Service::class,'receipt_service_details','receipt_id','service_id')
            ->withPivot('price','quantity');
    }

    public function room(): BelongsToMany
    {
        return $this->belongsToMany(Room::class, 'receipt_room_details', 'receipt_id', 'room_id')
            ->withPivot('check_in_time', 'check_out_time');
    }

    public function receiptChangeLog(): HasMany
    {
        return $this->hasMany(ReceiptChangeLogs::class);
    }
}
