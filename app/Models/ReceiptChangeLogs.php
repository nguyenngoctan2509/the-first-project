<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ReceiptChangeLogs extends Model
{
    use HasFactory;
    protected $table = 'receipt_change_logs';
    protected $fillable = [
        'admin_id',
        'status',
        'note'
    ];

    public function receipt(): BelongsTo
    {
        return $this->belongsTo(Receipt::class);
    }
}
