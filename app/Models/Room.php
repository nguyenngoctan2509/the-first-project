<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Room extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'rooms';
    protected $fillable=[
        'name',
        'status'
    ];

    protected function status(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value ? 'Có' : 'Không' ,
        );
    }

    public function roomType(): HasOne
    {
        return $this->hasOne(RoomType::class,'id','room_type_id');
    }
    public function viewType(): HasOne
    {
        return $this->hasOne(ViewType::class,'id','view_type_id');
    }

    public function receipt(): BelongsToMany
    {
        return $this->belongsToMany(Receipt::class,'receipt_room_details','room_id','receipt_id')
            ->withPivot('check_in_time','check_out_time');
    }

}
