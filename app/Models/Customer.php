<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Customer extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'customers';
    protected $fillable = [
        'name',
        'birthday',
        'email',
        'tier'
    ];

    protected function tier(): Attribute
    {
        return Attribute::make(
            get: fn($value) => ($value <= 1) ? 'below 5M' : (($value === 2) ? '5M-10M' : '10m< up'),
        );
    }

    public function totalMoney()
    {
        $startDate=Carbon::now()->subDays(14);
        $endDate = Carbon::now()->addDays(1);
        return $this->receipt()->whereBetween('created_at', [$startDate, $endDate])->get()
            ->sum(function ($receipt) {
                return $receipt->service->sum(function ($service) {
                    return $service->pivot->price * $service->pivot->quantity;
                });
        });
    }


    protected function birthday(): Attribute
    {
        return Attribute::make(
            get: fn($value) => Carbon::parse($value)->format('d/m/Y'),
        );
    }

    public function receipt(): HasMany
    {
        return $this->hasMany(Receipt::class);
    }
}
