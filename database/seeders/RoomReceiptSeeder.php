<?php

namespace Database\Seeders;

use App\Models\Receipt;
use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomReceiptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 3; $i++) {
            DB::table('receipt_room_details')->insert([
                'receipt_id' =>  DB::table('receipts')->inRandomOrder()->value('id'),
                'room_id' => DB::table('rooms')->inRandomOrder()->value('id'),
                'room_price' =>  fake()->numberBetween(100000, 1000000),
                'view_price' => fake()->numberBetween(100000, 1000000),
                'check_in_time' => Carbon::now('Asia/Ho_Chi_Minh'),
                'check_out_time' => Carbon::now('Asia/Ho_Chi_Minh')
            ]);
        }
    }
}
