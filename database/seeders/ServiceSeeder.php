<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 3; $i++) {
            DB::table('services')->insert([
                'name' => fake()->name(),
                'price' => fake()->numberBetween(100000, 1000000),
                'unit' => fake()->numberBetween(1, 20),
                'status' => fake()->boolean()
            ]);
        }
    }
}
