<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ViewTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 3; $i++) {
            DB::table('view_types')->insert([
                'name' => fake()->name(),
                'price' => fake()->numberBetween(100000, 1000000),
                'status' => fake()->boolean()
            ]);
        }
    }
}
