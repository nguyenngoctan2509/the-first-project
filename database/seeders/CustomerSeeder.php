<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 3; $i++) {
            DB::table('customers')->insert([
                'name' => fake()->name(),
                'birthday' => fake()->date(),
                'email' => fake()->unique()->email(),
                'tier' => 0,
            ]);
        }
    }
}
