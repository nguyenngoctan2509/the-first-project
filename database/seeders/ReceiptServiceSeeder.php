<?php

namespace Database\Seeders;

use App\Models\Receipt;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReceiptServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 3; $i++) {
            DB::table('receipt_service_details')->insert([
                'receipt_id' => DB::table('receipts')->inRandomOrder()->value('id'),
                'service_id' => DB::table('services')->inRandomOrder()->value('id'),
                'price' =>  fake()->numberBetween(100000, 1000000),
                'quantity' => fake()->numberBetween(1, 20),
                'created_at' => Carbon::now('Asia/Ho_Chi_Minh'),
                'updated_at' => Carbon::now('Asia/Ho_Chi_Minh')
            ]);
        }
    }
}
