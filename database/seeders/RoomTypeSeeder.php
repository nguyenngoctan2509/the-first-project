<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 3; $i++) {
            DB::table('room_types')->insert([
                'name' => fake()->name(),
                'price' => fake()->numberBetween(100000, 1000000),
                'status' => fake()->boolean()
            ]);
        }
    }
}
