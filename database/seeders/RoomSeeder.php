<?php

namespace Database\Seeders;

use App\Models\RoomType;
use App\Models\ViewType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 3; $i++) {
            DB::table('rooms')->insert([
                'room_type_id' => RoomType::factory()->create()->id,
                'name' => fake()->name(),
                'status' => fake()->boolean(),
                'view_type_id' => ViewType::factory()->create()->id,
            ]);
        }
    }
}
