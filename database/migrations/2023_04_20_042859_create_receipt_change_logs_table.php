<?php

use App\Models\Receipt;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('receipt_change_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('admin_id');
            $table->foreignIdFor(Receipt::class)->constrained()->onDelete('cascade');
            $table->boolean('status');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('receipt_change_logs');
    }
};
