<?php

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ReceiptController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\ServiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('rooms', RoomController::class)->middleware('auth.api');
Route::get('/services/{id}',[ServiceController::class,'countServiceById'])->name('service.count');
Route::get('/receipts/{id}',[ReceiptController::class,'calcMoneyBillById'])->name('receipt.calc')->middleware('auth.api');
Route::get('/customer/{room_id}/{start_time?}/{end_time?}',[CustomerController::class,'getCustomerOrderedRoomInATimeRange'])->name('customer.list')->middleware('auth.api');
Route::put('/receipts/change/{id}',[ReceiptController::class,'changeStatusReceiptById'])->name('receipt.change')->middleware('auth.api');
Route::post('/login',[AuthenticateController::class,'login'])->name('login');